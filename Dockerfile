FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN mkdir /opt/app
WORKDIR /opt/app

COPY . /opt/app

RUN python app.py
